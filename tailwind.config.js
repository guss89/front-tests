/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily:{
        'fredoka':'Fredoka One'
      },
      keyframes: {
        'fade-in-down': {
            '0%': {
                opacity: '0',
                transform: 'translateY(-10px)'
            },
            '100%': {
                opacity: '1',
                transform: 'translateY(0)'
            },
        },
        'menu-visible': {
          'from': {
            left: '125%'
          },
          'to': {
            left: '75%'
          }
        },
        'menu-hidden': {
          'from': {
            right: '0%'
          },
          'to': {
            right: '-25%'
          }
        }
      },
      animation: {
          'fade-in-down': 'fade-in-down 1s ease-out',
          'menu-visible': 'menu-visible 1s',
          'menu-hidden': 'menu-hidden 1s',
      }
    },
  },
  plugins: [
  ],
}
