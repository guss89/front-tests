import axios from "axios"
const state = {
    countries: [],
    teams:[],
    seasons:[],
    matches:[]
}

const getters = {
    getCountries: state => {
        return state.countries;
    },
    getTeams: state => {
        return state.teams;
    },
    getSeasons: state => {
        return state.seasons;
    },
    getMatches: state => {
        return state.matches;
    }
}

const mutations = {
    async loadCountries(state){
        try {
            await axios
            .get('https://app.sportdataapi.com/api/v1/soccer/countries?apikey=21133760-47e7-11ed-a371-d119b4162334')
            .then(response => {
                state.countries = response.data.data;
            });
            
        } catch (error) {
            console.error(error);
        }
    },
    async loadTeam(state, payload){
        try {
            await axios
            .get('https://app.sportdataapi.com/api/v1/soccer/teams?apikey=21133760-47e7-11ed-a371-d119b4162334&country_id='+payload)
            .then(response => {
                state.teams = response.data.data;
            });
            
        } catch (error) {
            console.error(error);
        }
    },
    async loadSeasons(state, payload){
        try {
            await axios
            .get('https://app.sportdataapi.com/api/v1/soccer/seasons?apikey=21133760-47e7-11ed-a371-d119b4162334&league_id='+payload)
            .then(response => {
                state.seasons = response.data.data;
            });
            
        } catch (error) {
            console.error(error);
        }
    },
    async loadMatches(state, payload){
        try {
            await axios
            .get('https://app.sportdataapi.com/api/v1/soccer/matches?apikey=21133760-47e7-11ed-a371-d119b4162334&season_id='+payload)
            .then(response => {
                state.matches = response.data.data;
            });
            
        } catch (error) {
            console.error(error);
        }
    },
    async filterByRangeDate(state, payload){
        try {
            await axios
            .get('https://app.sportdataapi.com/api/v1/soccer/matches?apikey=21133760-47e7-11ed-a371-d119b4162334&season_id='+payload.season+'&date_from='+payload.start+'&date_to='+payload.end)
            .then(response => {
                state.matches = response.data.data;
            });
            
        } catch (error) {
            console.error(error);
        }
    },
    async filterForLiveMatch(state){
        try {
            await axios
            .get('https://app.sportdataapi.com/api/v1/soccer/matches?apikey=21133760-47e7-11ed-a371-d119b4162334&live=true')
            .then(response => {
                state.matches = response.data.data;
            });
            
        } catch (error) {
            console.error(error);
        }
    }
}

const actions = {
    
    async findTeamByCountry(context,country){
        context.commit('loadTeam', country)
    },
    async findSeasonByLeague(context, league){
        context.commit('loadSeasons',league)
    },
    async findMatchBySeason(context, season){
        context.commit('loadMatches', season);
    },
    async findMatchLive(context){
        context.commit('filterForLiveMatch')
    },
    async filterByDate(context, filter){
        context.commit('filterByRangeDate',filter)
    }
}



export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}