const state = {
    favorites: [],
    history:[],
    puppiesList: [
        {
            url: '../assets/husky-1.jpg',
            breed: 'Husky',
            name:'Cherry',
            age: 'Three Months',
            id:1
        },
        {
            url: '../assets/husky-2.jpg',
            breed: 'Husky',
            name:'Draco',
            age: 'Three Months',
            id:2
        },
        {
            url: '../assets/husky-3.jpg',
            breed: 'Husky',
            name:'Blue',
            age: 'Three Months',
            id:3
        },
        {
            url: '../assets/husky-4.jpg',
            breed: 'Husky',
            name:'Dante',
            age: 'Three Months',
            id:4
        },
        {
            url: '../assets/beagle-1.jpg',
            breed: 'Beagle',
            name:'Terry',
            age: 'Three Months',
            id:5
        },
        {
            url: '../assets/beagle-2.jpg',
            breed: 'Beagle',
            name:'Volt',
            age: 'Three Months',
            id:6
        },
        {
            url: '../assets/labrador-2.jpg',
            breed: 'Labrador',
            name:'Boby',
            age: 'Three Months',
            id:9
        },
        {
            url: '../assets/labrador-3.jpg',
            breed: 'Labrador',
            name:'Till',
            age: 'Three Months',
            id:10
        },
        {
            url: '../assets/beagle-3.jpg',
            breed: 'Beagle',
            name:'Telmo',
            age: 'Three Months',
            id:7
        },
        {
            url: '../assets/labrador-1.jpg',
            breed: 'Labrador',
            name:'Tacos',
            age: 'Three Months',
            id:8
        },
    ],
    auxPuppiesList:[]
}

const getters = {
    getPuppies: state => {
        return state.puppiesList;
    },
    getFavorites: state => {
        return state.favorites;
    },
    getHistory: state => {
        return state.history;
    }
}

const mutations = {
    changeStateFavorites(state, item){
        let foundFavorite = state.favorites.findIndex(favorite => favorite.id === item.id);
        if(foundFavorite === -1){
            state.favorites.push(item);
        }else{
            state.favorites.splice(foundFavorite,1);
        }
        localStorage.setItem('favorites', JSON.stringify(state.favorites));
    },
    setFavorites(state, favorites){
        state.favorites = favorites;
    },
    setHistory(state, history){
        state.history = history
    },
    findPuppie(state, searchValue){
        if(searchValue.trim().length === 0){
            state.puppiesList = state.auxPuppiesList;
        }

        let tmpSearch = searchValue.toLowerCase();
        let foundPuppies = state.auxPuppiesList.filter((search) => {
            return search.breed.toLowerCase().indexOf(tmpSearch) !== -1 || !tmpSearch
        });
        
        let lastSearch = state.history.length;
        state.history.push({text:searchValue,id:lastSearch + 1});
        localStorage.setItem('history', JSON.stringify(state.history))

        state.puppiesList = foundPuppies;
    },
    initAuxPuppiesList(state){
        state.auxPuppiesList = state.puppiesList;
    }
}

const actions = {
}



export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}