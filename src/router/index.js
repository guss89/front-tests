import Vue from 'vue'
import Router from 'vue-router'

import Puppies from '@/views/Puppies.vue'
import Sports from '@/views/Sports.vue'
import SportTeams from '@/views/SportTeams.vue'
import SportMatches from '@/views/SportMatches.vue'

Vue.use(Router);

const routes = [
    {
        path:'/',
        component: Puppies,
        name: 'puppies'
    },
    {
        path:'/puppies',
        component: Puppies,
        name: 'puppies'
    },
    {
        path:'/sports',
        component: Sports,
        name: 'sports',
        children:[
            {
                path:'/',
                component: SportTeams,
                name:'teams'
            },
            {
                path:'/teams',
                component: SportTeams,
                name:'teams'
            },
            {
                path:'/matches',
                component: SportMatches,
                name:'matches'
            }
        ]
    }
];

var router = new Router({
    routes: routes
});

export default router;